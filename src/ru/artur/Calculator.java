package ru.artur;

/**
 * Класс Калькулятор, выполняет все арифметические действия.
 *
 * @author - Олшаускис Артур - 13ОИТ18к
 */
public class Calculator {

    /**
     * Метод сложения чисел.
     *
     * @param oneValue - Первое число.
     * @param twoValue - Второе число.
     * @return Результат сложения.
     */
    public static double addition(double oneValue, double twoValue) {
        return oneValue + twoValue;
    }

    /**
     * Метод вычитания чисел.
     *
     * @param oneValue - Первое число.
     * @param twoValue - Второе число.
     * @return Результат вычитания.
     */
    public static double subtraction(double oneValue, double twoValue) {
        return oneValue - twoValue;
    }

    /**
     * Метод умножение чисел.
     *
     * @param oneValue - Первое число.
     * @param twoValue - Второе число.
     * @return Результат умножения.
     */
    public static double multiplication(double oneValue, double twoValue) {
        return oneValue * twoValue;
    }

    /**
     * Метод возведения в степень.
     *
     * @param oneValue - Первое значение
     * @param twoValue - Второе значение.
     * @return Результат возведения в степень.
     */
    public static double pow(double oneValue, double twoValue) {
        if (twoValue == 0) return 1;
        if (twoValue == 1) return oneValue;
        if (twoValue < 0) {
            twoValue = 0 - twoValue;
            return 1 / powHelper(oneValue, twoValue);
        }
        return powHelper(oneValue, twoValue);
    }

    /**
     * Вспомогательный метод возведения в степень.
     *
     * @param oneValue - Первое значение
     * @param twoValue - Второе значение
     * @return число в результате возведения.
     */
    private static double powHelper(double oneValue, double twoValue) {
        double temporary = oneValue;
        for (int i = 1; i < twoValue; i++) {
            oneValue = temporary * oneValue;
        }
        return oneValue;
    }

    /**
     * Метод деления вещественных чисел.
     *
     * @param oneValue - Первое значение.
     * @param twoValue - Второе значение.
     * @return Результат деления.
     */
    public static double division(double oneValue, double twoValue) {
        if (twoValue == 0) throw new ArithmeticException("Деление на ноль невозможно!");
        return oneValue / twoValue;
    }

    /**
     * Метод деления целых чисел.
     *
     * @param oneValue - Первое значение.
     * @param twoValue - Второе значение.
     * @return Результат деления.
     */
    public static int division(int oneValue, int twoValue) {
        if (twoValue == 0) throw new ArithmeticException("Деление на ноль невозможно!");
        return oneValue / twoValue;
    }

    /**
     * Метод деления чисел с остатком.
     *
     * @param oneValue - Первое значение.
     * @param twoValue - Второе значение.
     * @return Результат деления.
     */
    public static double divisionRemainder(double oneValue, double twoValue) {
        if (twoValue == 0) throw new ArithmeticException("Деление на ноль невозможно!");
        return oneValue % twoValue;
    }

}
