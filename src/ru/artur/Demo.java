package ru.artur;

/**
 * Демонстрационный класс.
 * Выполняет методы ввода данных с клавиатуры/файла, вывод на экран/файл.
 *
 * @author - Олшаускис Артур - 13ОИТ18к
 */

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.io.*;

public class Demo {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        String input, symbol, temporary;
        double oneValue, twoValue;

        System.out.print("Введите значения для калькулятора: ");
        temporary = sc.nextLine();

        if (patternCheck(temporary)) {
            String splitKeyboard[] = temporary.split(" ");

            oneValue = Double.parseDouble(splitKeyboard[0]);
            twoValue = Double.parseDouble(splitKeyboard[1]);
            symbol = splitKeyboard[2];

            System.out.println(outputDate(oneValue, twoValue, symbol));
        } else {
            System.out.println("Данные были введены не корректно!");
        }

        System.out.println("Инициализация данных с файла...");

        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader("inputDate.txt"));
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("outputDate.txt"));

            while ((input = bufferedReader.readLine()) != null) {
                if (patternCheck(input)) {
                    String splitValueKeyboard[] = input.split(" ");

                    oneValue = Double.parseDouble(splitValueKeyboard[0]);
                    twoValue = Double.parseDouble(splitValueKeyboard[1]);
                    symbol = splitValueKeyboard[2];

                    String test = outputDate(oneValue, twoValue, symbol);

                    bufferedWriter.write(test + "\n");
                } else {
                    bufferedWriter.write("Исходнные данные были введены не корректно!\n");
                }
            }
            bufferedReader.close();
            bufferedWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("Данные отправлены в файл!");
    }

    /**
     * Метод позволяющий проверить корректность исходных данных.
     *
     * @param value - Строка с введеными данными.
     * @return true/false
     */
    private static boolean patternCheck(String value) {
        Pattern pattern = Pattern.compile("^[+-]?\\d+(\\.\\d+)?\\s?[+-]?\\d+(\\.\\d+)?\\s?[+-/*^%]$");
        Matcher m = pattern.matcher(value);

        return m.matches();
    }

    /**
     * Метод вывода ответов, вычисленных выражений.
     *
     * @param oneValue - Первое значение.
     * @param twoValue - Второе значение.
     * @param symbol   - Символ(Знак действия).
     * @return ответ.
     */
    private static String outputDate(double oneValue, double twoValue, String symbol) {
        switch (symbol) {
            case "+":
                return "Сложение: " + Calculator.addition(oneValue, twoValue);
            case "-":
                return "Вычитание: " + Calculator.subtraction(oneValue, twoValue);
            case "*":
                return "Умножение: " + Calculator.multiplication(oneValue, twoValue);
            case "/":
                return "Деление: " + Calculator.division(oneValue, twoValue);
            case "%":
                return "Деление с остатком: " + Calculator.divisionRemainder(oneValue, twoValue);
            case "^":
                return "Возведение в степень: " + Calculator.pow(oneValue, twoValue);
            default:
                return "Данные были введены не корректно!";
        }
    }
}
